import React,{useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import {connect} from 'react-redux'
import { useHistory } from "react-router-dom";
import UserDetails from '../UserDetails/UserDetails';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
    textAlign:'center'
  },
});



const DTable = (props) => {

    const [isOpen,setIsOpen]= useState(false)

  
  const classes = useStyles();
  console.log(props.Users)
  
const deleteHandler=(index)=>{
    props.User.slice(index,1)
    console.log("Fun Call")
}

let history = useHistory();

const Add=()=>{
    history.push("/Contact");    
}
  return (
   <>   
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell align="right">NAME</TableCell>
            <TableCell align="right">Email</TableCell>
            <TableCell align="right">Date OF MODIFICATION</TableCell>
            <TableCell align="right">DATE OF CREATION</TableCell>
            <TableCell align="right">Actions</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.User.map((users,index) => (
            
            <TableRow key={index}>
                {console.log(index)}
              <TableCell component="th" scope="row">
                {users.name}
              </TableCell>
          
              <TableCell onClick={(index)=>{deleteHandler(index)}}  align="right">{users.Email}</TableCell>
              <TableCell  align="right">{users.date_of_MOdification}</TableCell>
              <TableCell  align="right">{users.date_of_Creation}</TableCell>
              <TableCell  align="right">

                  <button style={{marginRight:'4px'}} className="btn btn-primary btn-md" onClick={()=>{Add()} }>+</button>
                  <button style={{marginRight:'4px'}} className="btn btn-success btn-md" onClick={(index)=>{props.delUser(index)}}>-</button>
                  <button onClick={()=>{setIsOpen(true)}} style={{marginRight:'4px'}} className="btn btn-info btn-md">view</button>
              </TableCell>
              <TableCell style={{width:"30%",height:'50%'}} align="right">
            
              <UserDetails 
                  open={isOpen}
                  onClose={()=>{setIsOpen(false)}}
                  name={users.name}
                  Email={users.Email}
                  DOC={users.date_of_Creation}
                  DOM={users.date_of_MOdification}
                  >
               fancybox
           </UserDetails>
 

              </TableCell>
            </TableRow>
            
          ))
          }
       </TableBody>
        
      </Table>
    </TableContainer>

           
          
</>
  );
}


const mapStateToProps = (state) =>{
    return {
        User:state.user
    }
}

const mapDispatchToProps = (dispatch) =>{
    return{
        delUser:(index)=>{dispatch({
            type:'DEL_USER',
            payload:{
               id:index
            }})}
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(DTable);