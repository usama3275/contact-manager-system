import React from 'react'
import {connect} from 'react-redux'
import DTable from '../Table/Table.js'
const Home=(props)=>{
    console.log(props)
        
    return(
        <div className="container">
            
            <div className='py-4'>
                <h1 style={{textAlign:'center'}}>Home Contact Page</h1>
                <DTable/>


            </div>
        </div>
    )
}

const mapStateToProps = (state) =>{
    return {
        User:state.user
    }
}

export default connect(mapStateToProps)(Home); 