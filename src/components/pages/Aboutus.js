import React from 'react'

const Aboutus=()=>{
    return(
        <div className="container">
            <div className='py-4'>
                <h1 style={{textAlign:'center'}}>About Page</h1>
                
                <p className="font-weight-light" style={{fontSize:'20px',textAlign:'center'}}>Contact management is the process of recording contacts’ details and tracking their interactions with a business. Such systems have gradually evolved into an aspect of customer relationship management (CRM) systems, which allow businesses to improve sales and service levels leveraging a wider range of data.</p>
                <p className="font-weight-light" style={{fontSize:'20px',textAlign:'center'}} > Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
            </div>
        </div>
    )
}

export default Aboutus;