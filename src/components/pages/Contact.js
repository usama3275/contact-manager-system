import React,{useState} from 'react'
import {connect} from 'react-redux'




const Contact=(props)=>{
    
    const [name, setName] = useState("name");
    const [Email, setEmail] = useState("Email");
    const [date_Of_Mod, setDate_Of_Mod] = useState("Date");
    const [date_Of_Creation, setDate_Of_Creation] = useState("Date");


 
    console.log(Email,name,date_Of_Creation,date_Of_Mod)
    return(
        <div className="container">
            <div className='py-4'>
                <h1>ADD Contact Page</h1>
                <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" className="form-control" onChange={e => setName(e.target.value)}  aria-describedby="emailHelp"/>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" className="form-control" onChange={e => setEmail(e.target.value)}  aria-describedby="emailHelp"/>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Date OF Modification</label>
                        <input type="text" onChange={e => setDate_Of_Mod(e.target.value)} className="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Date_OF_Creation</label>
                        <input type="text" onChange={e => setDate_Of_Creation(e.target.value)}  className="form-control"  aria-describedby="emailHelp"/>
                    
                    </div>
                    <div style={{ paddingTop:'10px' }}>
                         <button type="button" className="btn btn-primary btn-lg" onClick={()=>{props.addUser(name,Email,date_Of_Mod,date_Of_Creation)}} >Submit</button>
                    </div>
             
                </form>
            </div>
        </div>
    );
}

const mapDispatchToProps = (dispatch) =>{
    return{
        addUser:(name,Email,date_Of_Mod,date_Of_Creation)=>{dispatch({
            type:'ADD_USER',
            payload:{
                name:name,
                Email:Email,
                date_of_MOdification:date_Of_Mod,
                date_of_Creation:date_Of_Creation
            }})}
    }
}

export default connect(null,mapDispatchToProps)(Contact);