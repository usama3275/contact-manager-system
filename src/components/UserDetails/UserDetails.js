import React ,{alert}from 'react'
import './Model.css'

function UserDetails(props ){
if(!props.open) return null
   
    return(
        <div  className="Model">
            <button className="Btn" onClick={props.onClose}>Close</button>
           
            <p>name:{props.name}</p>
            <p>Email:{props.Email}</p>
            <p>Date_Of-Creation:{props.DOC}</p>
            <p>Date of Modification:{props.DOM}</p>
        </div>
    )
}

export default UserDetails