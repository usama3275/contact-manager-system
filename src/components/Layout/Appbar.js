import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import './Appbar.css'
import {NavLink} from 'react-router-dom'
import Link from '@material-ui/core/Link';
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));



export default function ButtonAppBar() {
  const classes = useStyles();
 
  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
            
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            CONTACT MANAGER
          </Typography>
          <div className="nav">
                 <li>
                    <NavLink to="/">Home</NavLink>
                 </li>
                 <li>
                     <NavLink to="/Aboutus">Aboutus</NavLink>
                 </li>
                 <li>
                     <NavLink to="/Contact">Contact</NavLink>
                 </li>
                 
                 </div>
                
        </Toolbar>
      </AppBar>
    </div>
  );
}


