import React from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.css'
import Home from './components/pages/Home'
import Aboutus from './components/pages/Aboutus'
import Contact from './components/pages/Contact'
import Appbar from './components/Layout/Appbar'
import {BrowserRouter,Route,Switch} from 'react-router-dom'
import P404 from './components/pages/P404'
function App() {
  return (
    <BrowserRouter>
      <div className="App">
          <Appbar/>
          <Switch>
            <Route exact path='/' component={Home}/>
            <Route exact path='/Aboutus' component={Aboutus}/>
            <Route exact path='/Contact' component={Contact}/>
            <Route component={P404}/>
          </Switch>
       
      </div>
 
    </BrowserRouter>
  );
}

export default App;
